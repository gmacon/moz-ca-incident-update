# Mozilla CA Incident Update Watcher

## Overview

This tool examines the currently-open CA Incident bugs on Bugzilla
and provides a list of those that should have been updated by now but haven't been.
The list provided is an approximation,
so you should verify that the CA is noncompliant before complaining about them.

Here's the [relevant section][Keeping Us Informed] of the Incident Response policy:

> # Keeping Us Informed
>
> Once the report is posted,
> you should respond promptly to questions that are asked,
> and in no circumstances should a question linger without a response for more than one week,
> even if the response is only to acknowledge the question and provide a later date when an answer will be delivered.
> You should also provide updates at least every week giving your progress,
> and confirm when the remediation steps have been completed -
> unless Mozilla representatives agree to a different schedule by setting a “Next Update” date in the “Whiteboard” field of the bug.
> Such updates should be posted to the m.d.s.p. thread, if there is one, and the Bugzilla bug.
> The bug will be closed when remediation is completed.

This tool uses the query from the [CA Incident Dashboard][] to get the list of open bugs.
The Next Update date is computed as the later of
the date parsed from the Whiteboard "Next Update" comment or
Last Update + 7 days.

A report is printed, grouped by CA, listing all bugs where the Next Update date is in the past.

[Keeping Us Informed]: https://wiki.mozilla.org/CA/Responding_To_An_Incident#Keeping_Us_Informed
[CA Incident Dashboard]: https://wiki.mozilla.org/CA/Incident_Dashboard

## Running the Tool

This code targets Python 3.7 and is packaged with [Poetry][].
With Poetry installed, you can clone this repository, change directory into it, and execute

```bash
poetry install
poetry run ./moz_ca_incident_update.py
```

The report will be printed to standard output.

[Poetry]: https://poetry.eustace.io/
