#!/usr/bin/env python3

import datetime
import re
from collections import defaultdict

import click
from appdirs import user_cache_dir
from cachecontrol import CacheControl
from cachecontrol.caches.file_cache import FileCache
from dateutil.parser import isoparse, parse
from requests import Session

BUGZILLA_QUERY = (
    "https://bugzilla.mozilla.org/rest/bug?"
    "include_fields=id,summary,status&"
    "cf_blocking_b2g_type=contains&"
    "component=CA%20Certificate%20Compliance&"
    "include_fields=id&"
    "include_fields=summary&"
    "include_fields=status&"
    "include_fields=assigned_to&"
    "include_fields=whiteboard&"
    "include_fields=last_change_time&"
    "status=UNCONFIRMED&"
    "status=NEW&"
    "status=ASSIGNED&"
    "status=REOPENED&"
    "whiteboard=ca-compliance"
)
BUGZILLA_BUG_URL = "https://bugzilla.mozilla.org/show_bug.cgi?id={}"
NEXT_UPDATE = re.compile("next update", re.IGNORECASE)


def parse_report(today):
    sess = CacheControl(
        Session(),
        cache=FileCache(user_cache_dir("moz_ca_incident_update", "George Macon")),
    )

    resp = sess.get(BUGZILLA_QUERY)
    resp.raise_for_status()
    data = resp.json()

    reports = defaultdict(list)

    for bug in data["bugs"]:
        bugid = bug["id"]
        if ":" in bug["summary"]:
            ca, summary = bug["summary"].split(":", 1)
        else:
            ca = "Unknown"
            summary = bug["summary"]
        whiteboard = bug["whiteboard"]
        last_update = isoparse(bug["last_change_time"]).date()
        next_update = last_update + datetime.timedelta(days=7)
        m = NEXT_UPDATE.search(whiteboard)
        if m:
            next_update = max(parse(whiteboard[m.end() :]).date(), next_update)
        if next_update < today:
            reports[ca].append((bugid, summary.strip(), next_update, last_update))
    return reports


def print_report(today, reports):
    print("# CA Incident bugs with Next Update in the past")
    print("")
    print(f"Report run {today:%Y-%m-%d}.")
    for ca, bugs in sorted(reports.items()):
        print("")
        print(f"## {ca}")
        print("")
        for bugid, summary, next_update, last_update in sorted(bugs):
            print(f"*   {summary}")
            print(f"    Next Update: {next_update:%Y-%m-%d}")
            print(f"    Last Update: {last_update:%Y-%m-%d}")
            print(f"    {BUGZILLA_BUG_URL.format(bugid)}")


@click.command()
def main():
    today = datetime.date.today()
    print_report(today, parse_report(today))


if __name__ == "__main__":
    main()
